# Contribution Guide

Contributions are welcome, provided that they follow OOP best practices
and Java conventions.

You can find in the `config` folder some Eclipse configuration files so 
that your code can gracefully be merged with the rest of the plug-in code.

Don't hesitate to add your name in the header of the source files you
edit.

Of course, commit messages **must** use Gitmojis!

To contribute, just fork this project, and ask for a merge request when 
you think that you have developed a great feature for our plug-in, or 
fixed a bug in it.
