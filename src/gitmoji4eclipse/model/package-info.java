/**
 * The {@code gitmoji4eclipse.model} package contains the classes used to handle
 * the representation and the loading of all possible Gitmojis.
 *
 * @author Romain WALLON
 *
 * @version 1.1
 */

package gitmoji4eclipse.model;
