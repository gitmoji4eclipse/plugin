/*******************************************************************************
 * Copyright (C) 2018-2021, Romain WALLON <romain.gael.wallon@gmail.com>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package gitmoji4eclipse.model;

import org.eclipse.jgit.annotations.NonNull;

/**
 * A Gitmoji provides all the data needed to choose the most appropriate
 * Gitmoji and use it in a commit message.
 *
 * @author Romain WALLON
 *
 * @version 1.1
 * @since 1.0
 */
public final class Gitmoji {

	/**
	 * The emoji of this Gitmoji.
	 */
	private final String emoji;

	/**
	 * The code of this Gitmoji, to be used in a commit message.
	 */
	private final String code;

	/**
	 * The description of this Gitmoji, detailing when to use it.
	 */
	private final String description;

	/**
	 * Creates a new Gitmoji.
	 *
	 * @param emoji The emoji of the Gitmoji.
	 * @param code The code of the Gitmoji, to be used in a commit message.
	 * @param description The description of the Gitmoji, detailing when to use it.
	 */
	Gitmoji(@NonNull String emoji, @NonNull String code, @NonNull String description) {
		this.emoji = emoji;
		this.code = code;
		this.description = description;
	}

	/**
	 * Gives the emoji of this Gitmoji.
	 *
	 * @return The emoji of this Gitmoji.
	 */
	public String getEmoji() {
		return emoji;
	}

	/**
	 * Gives the code of this Gitmoji, to be used in a commit message.
	 *
	 * @return The code of this Gitmoji.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * Gives the description of this Gitmoji, detailing when to use it.
	 *
	 * @return The description of when to use this Gitmoji.
	 */
	public String getDescription() {
		return description;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return emoji + " " + description;
	}

}
