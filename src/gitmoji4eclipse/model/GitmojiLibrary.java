/*******************************************************************************
 * Copyright (C) 2018, Romain WALLON <romain.gael.wallon@gmail.com>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package gitmoji4eclipse.model;

import static gitmoji4eclipse.Activator.log;

import java.io.IOException;
import java.io.InputStream;
import java.net.Proxy;
import java.net.ProxySelector;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jgit.util.HttpSupport;

/**
 * A utility class to lazily download the Gitmojis from the
 * <a href="https://github.com/carloscuesta/gitmoji/">GitHub repository</a>, and
 * to keep a reference to them.
 * 
 * @author Romain WALLON
 * 
 * @version 1.0.1
 * @since 1.0
 */
public final class GitmojiLibrary {

	/**
	 * The URL of the JSON file in which the Gitmojis are stored.
	 */
	private static final String GITMOJIS_URL = "https://gitmoji.dev/api/gitmojis";

	/**
	 * The collection of all existing Gitmojis.
	 * They are lazily downloaded from the GiHub repository.
	 */
	private static final List<Gitmoji> ALL_GITMOJIS = new LinkedList<>();

	/**
	 * Disables instantiation.
	 */
	private GitmojiLibrary() {
		throw new AssertionError("No GitmojiLibrary instances for you!");
	}

	/**
	 * Gives the collection of all existing Gitmojis.
	 *
	 * @return The collection of Gitmojis. 
	 *         An empty collection is returned when download fails.
	 */
	public static Collection<Gitmoji> gitmojis() {
		if (ALL_GITMOJIS.isEmpty()) {
			download();
		}

		return Collections.unmodifiableList(ALL_GITMOJIS);
	}

	/**
	 * Downloads the Gitmojis from the GitHub project.
	 */
	private static final void download() {
		try (GitmojiJsonReader reader = new GitmojiJsonReader(openStream())) {
			ALL_GITMOJIS.addAll(reader.read());

		} catch (IOException e) {
            log(IStatus.ERROR, e);
		}
	}

	/**
	 * Opens a stream to read the remote file containing the description of the
	 * Gitmojis. 
	 * If a network proxy is defined, it is used to get the connection.
	 *
	 * @return The stream to use to read the file.
	 *
	 * @throws IOException If the connection used to get the file fails.
	 */
	private static InputStream openStream() throws IOException {
		URL url = new URL(GITMOJIS_URL);
		Proxy proxy = HttpSupport.proxyFor(ProxySelector.getDefault(), url);

		URLConnection connection = url.openConnection(proxy);
		return connection.getInputStream();
	}

}
