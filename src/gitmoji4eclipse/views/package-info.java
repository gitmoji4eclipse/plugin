/**
 * The {@code gitmoji4eclipse.views} package contains the classes of the graphical
 * components of the plug-in.
 *
 * @author Thibault FALQUE
 * @author Romain WALLON
 *
 * @version 1.1
 */

package gitmoji4eclipse.views;
