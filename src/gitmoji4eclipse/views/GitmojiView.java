/*******************************************************************************
 * Copyright (C) 2018, Thibault FALQUE <thibault.falque@gmail.com>
 * Copyright (C) 2018, Romain WALLON <romain.gael.wallon@gmail.com>
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package gitmoji4eclipse.views;

import java.util.List;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.part.ViewPart;

import gitmoji4eclipse.egit.ICommitMessageArea;
import gitmoji4eclipse.model.Gitmoji;
import gitmoji4eclipse.model.GitmojiLibrary;

/**
 * A view providing a large choice of Gitmojis, loaded from the dedicated
 * <a href="https://github.com/carloscuesta/gitmoji/">GitHub repository</a>.
 * 
 * @author Thibault FALQUE
 * @author Romain WALLON
 * 
 * @version 1.0.1
 * @since 1.0
 */
public final class GitmojiView extends ViewPart {

    /**
     * The ID of the view as specified by the extension.
     */
    public static final String ID = "gitmoji4eclipse.views.GitmojiView";

    /**
     * The provider used to get an array from the list of Gitmojis.
     */
    private static final IStructuredContentProvider PROVIDER = e -> ((List<?>) e).toArray();

    /**
     * The search field used to search for Gitmojis.
     */
    private Text searchField;

    /**
     * The list viewer showing the Gitmojis to the user.
     */
    private ListViewer list;

    /**
     * The filter used to filter the results of the search.
     */
    private final GitmojiViewerFilter filter;

    /**
     * The commit message area used to write commit messages.
     */
    private final ICommitMessageArea commitMessageArea;

    /**
     * Creates a new GitmojiView.
     */
    public GitmojiView() {
        this.filter = new GitmojiViewerFilter();
        this.commitMessageArea = ICommitMessageArea.defaultInstance();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.eclipse.ui.part.WorkbenchPart#createPartControl(org.eclipse.swt.
     * widgets. Composite)
     */
    @Override
    public void createPartControl(Composite parent) {
        GridLayout gridLayout = new GridLayout();
        gridLayout.numColumns = 1;
        parent.setLayout(gridLayout);
        parent.setBackground(new Color(parent.getDisplay(), 255, 255, 255));
        
        createSearchField(parent);
        createListViewer(parent);
    }

    /**
     * Creates the search field component.
     * 
     * @param parent The parent in which to put the component.
     */
    private void createSearchField(Composite parent) {
        searchField = new Text(parent, SWT.FILL);
        searchField.setMessage("Search for a Gitmoji");
        searchField.addModifyListener(e -> refreshList());
        searchField.setLayoutData(new GridData(SWT.FILL, SWT.NONE, true, false));
    }

    /**
     * Creates the list showing the Gitmojis.
     * 
     * @param parent The parent in which to put the component.
     */
    private void createListViewer(Composite parent) {
        list = new ListViewer(parent, SWT.V_SCROLL);
        list.setContentProvider(PROVIDER);
        list.setInput(GitmojiLibrary.gitmojis());
        list.getList().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        
        list.addFilter(filter);

        list.addDoubleClickListener(e -> {
            IStructuredSelection selection = (IStructuredSelection) e.getSelection();
            if (!selection.isEmpty()) {
                Gitmoji gitmoji = (Gitmoji) selection.toList().get(0);
                onGitmojiSelected(gitmoji);
            }
        });
    }

    /**
     * Refreshes the list to show only the matching Gitmojis.
     */
    private void refreshList() {
        filter.setSearchText(searchField.getText());
        list.refresh();
    }

    /**
     * Performs an action when a Gitmoji is selected.
     * 
     * @param gitmoji The selected Gitmoji.
     */
    private void onGitmojiSelected(Gitmoji gitmoji) {
        commitMessageArea.setCommitMessage(gitmoji.getCode() + " " + commitMessageArea.getCommitMessage());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.eclipse.ui.part.WorkbenchPart#setFocus()
     */
    @Override
    public void setFocus() {
        searchField.setFocus();
    }

}
