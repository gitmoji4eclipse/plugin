/*******************************************************************************
 * Copyright (C) 2018-2021, Thibault FALQUE <thibault.falque@gmail.com>
 * Copyright (C) 2018-2021, Romain WALLON <romain.gael.wallon@gmail.com>
 * 
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package gitmoji4eclipse.views;

import java.util.regex.Pattern;

import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerFilter;

import gitmoji4eclipse.model.Gitmoji;

/**
 * A filter to select Gitmojis by their description.
 *
 * @author Thibault FALQUE
 * @author Romain WALLON
 * 
 * @version 1.1
 * @since 1.0
 */
final class GitmojiViewerFilter extends ViewerFilter {

    /**
     * The search text entered by the user.
     */
    private String searchText;

    /**
     * Sets the search text entered by the user.
     *
     * @param searchText The search text to set.
     */
    public void setSearchText(String searchText) {
        this.searchText = ".*" + Pattern.quote(searchText) + ".*";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.eclipse.jface.viewers.ViewerFilter#select(org.eclipse.jface.viewers.Viewer,
     * java.lang.Object, java.lang.Object)
     */
    @Override
    public boolean select(Viewer viewer, Object parent, Object element) {
        if ((searchText == null) || searchText.isEmpty()) {
            return true;
        }

        Gitmoji gitmoji = (Gitmoji) element;
        return Pattern.compile(searchText, Pattern.CASE_INSENSITIVE)
                .matcher(gitmoji.getDescription())
                .find();
    }

}
