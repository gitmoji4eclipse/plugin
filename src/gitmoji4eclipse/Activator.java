/*******************************************************************************
 * Copyright (C) 2018-2021, Romain WALLON <romain.gael.wallon@gmail.com>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package gitmoji4eclipse;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

/**
 * The Activator class controls the plug-in life cycle.
 *
 * @author Romain WALLON
 *
 * @version 1.1
 * @since 1.0
 */
public final class Activator extends AbstractUIPlugin {

    /**
     * The plug-in ID.
     */
    public static final String PLUGIN_ID = "gitmoji4eclipse";

    /**
     * The shared instance of the plug-in.
     */
    private static Activator plugin;

    /**
     * Gives the shared instance of the plug-in.
     *
     * @return The shared instance of the plug-in.
     */
    public static Activator getDefault() {
        return plugin;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
     */
    @Override
    public void start(BundleContext context) throws Exception {
        super.start(context);
        plugin = this;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
     */
    @Override
    public void stop(BundleContext context) throws Exception {
        plugin = null;
        super.stop(context);
    }

    /**
     * Returns an image descriptor for the image file at the given
     * plug-in relative path
     *
     * @param path The path to the image.
     *
     * @return The image descriptor.
     */
    public static ImageDescriptor getImageDescriptor(String path) {
        return imageDescriptorFromPlugin(PLUGIN_ID, path);
    }

    /**
     * Logs a caught throwable.
     *
     * @param status The status to log the caught throwable with.
     * @param throwable The caught throwable.
     */
    public static void log(int status, Throwable throwable) {
        plugin.getLog().log(new Status(status, PLUGIN_ID, throwable.getMessage(), throwable));
    }

}
