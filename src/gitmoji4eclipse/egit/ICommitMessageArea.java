/*******************************************************************************
 * Copyright (C) 2018-2021, Romain WALLON <romain.gael.wallon@gmail.com>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package gitmoji4eclipse.egit;

/**
 * The ICommitMessageArea interface defines the contract for manipulating the
 * the commit message area of EGit, especially to get and set the message it
 * contains.
 *
 * @author Romain WALLON
 *
 * @version 1.1
 * @since 1.0
 */
public interface ICommitMessageArea {

    /**
     * Gives the commit message area provided by EGit.
     *
     * @return The commit message area provided by EGit.
     *
     * @since 1.1
     */
    public static ICommitMessageArea defaultInstance() {
        return EGitCommitMessageArea.instance();
    }

    /**
     * Gives the commit message written in this area.
     *
     * @return The commit message.
     */
    String getCommitMessage();

    /**
     * Sets the commit message to write in this area.
     *
     * @param message The commit message to write.
     */
    void setCommitMessage(String message);

}
