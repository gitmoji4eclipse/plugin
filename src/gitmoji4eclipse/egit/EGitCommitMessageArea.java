/*******************************************************************************
 * Copyright (C) 2018-2021, Romain WALLON <romain.gael.wallon@gmail.com>
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/

package gitmoji4eclipse.egit;

import static gitmoji4eclipse.Activator.log;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Optional;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

/**
 * The representation of EGit's commit message area, as an adapter of EGit's view.
 *
 * @author Romain WALLON
 *
 * @version 1.0.1
 * @since 1.0
 */
final class EGitCommitMessageArea implements ICommitMessageArea {

    /**
     * The single instance of this class.
     */
    private static final EGitCommitMessageArea INSTANCE = new EGitCommitMessageArea();

    /**
     * Creates a new EGitCommitMessageArea.
     */
    private EGitCommitMessageArea() {
        // Nothing to do: Singleton DP.
    }

    /**
     * Gives the single instance of EGitCommitMessageArea.
     * 
     * @return The single instance of EGitCommitMessageArea.
     */
    static ICommitMessageArea instance() {
        return INSTANCE;
    }

    /*
     * (non-Javadoc)
     *
     * @see gitmoji4eclipse.egit.ICommitMessageArea#getCommitMessage()
     */
    @Override
    public String getCommitMessage() {
        try {
            Optional<Object> commitMessageText = commitMessageText();
            
            if (commitMessageText.isPresent()) {
                // Reflectively invoking the method used to get the text.
                Method getText = commitMessageText.get().getClass().getMethod("getText");
                getText.setAccessible(true);
                return (String) getText.invoke(commitMessageText.get());
            }
            
            return "";

        } catch (ReflectiveOperationException e) {
            log(IStatus.ERROR, e);
            return "";
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see gitmoji4eclipse.egit.ICommitMessageArea#setCommitMessage(java.lang.String)
     */
    @Override
    public void setCommitMessage(String message) {
        try {
            Optional<Object> commitMessageText = commitMessageText();
            
            if (commitMessageText.isPresent()) {
                // Reflectively invoking the method used to set the text.
                Method setText = commitMessageText.get().getClass().getMethod("setText", String.class);
                setText.setAccessible(true);
                setText.invoke(commitMessageText.get(), message);
            }

        } catch (ReflectiveOperationException e) {
            log(IStatus.ERROR, e);
        }
    }

    /**
     * Gives the commit message text provided by EGit.
     * 
     * @return The commit message text provided by EGit, or an empty object if it could
     *         not be retrieved.
     */
    private Optional<Object> commitMessageText() {
        try {
            // Showing the staging view if needed.
            IViewPart stagingView = PlatformUI.getWorkbench()
                    .getActiveWorkbenchWindow()
                    .getActivePage()
                    .showView("org.eclipse.egit.ui.StagingView");

            // Retrieving the commit message text from the view.
            Field commitMessageText = stagingView.getClass().getDeclaredField("commitMessageText");
            commitMessageText.setAccessible(true);
            return Optional.of(commitMessageText.get(stagingView));

        } catch (PartInitException | ReflectiveOperationException e) {
            // The commit message text could not be retrieved.
            log(IStatus.ERROR, e);
            return Optional.empty();
        }
    }

}
