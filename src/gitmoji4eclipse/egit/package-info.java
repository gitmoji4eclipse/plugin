/**
 * The {@code gitmoji4eclipse.egit} package contains the classes used to communicate
 * with EGit.
 *
 * @author Romain WALLON
 *
 * @version 1.1
 */

package gitmoji4eclipse.egit;
