/**
 * The {@code gitmoji4eclipse} package is the main package of the plug-in.
 *
 * @author Thibault FALQUE
 * @author Romain WALLON
 *
 * @version 1.1
 */

package gitmoji4eclipse;
