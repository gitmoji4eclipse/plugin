# Gitmoji 4 Eclipse - Plug-In

This project provides the plug-in project of the **gitmoji4eclipse** plug-in.

The feature project is hosted [here](https://gitlab.com/gitmoji4eclipse/feature),
and the update-site project [here](https://gitlab.com/gitmoji4eclipse/update-site).

Get more information and an how-to on the
[update-site page](https://gitmoji4eclipse.gitlab.io/update-site).

## Overview

This plug-in makes easier the use of
[Gitmojis](https://gitmoji.carloscuesta.me/) in commit messages, by providing a
view that displays each emoji with a description of when it should be used,
according to what is to be committed.

This plug-in requires [EGit](http://www.eclipse.org/egit/), with at least
version 4.9.0.
It should be packaged with your Eclipse installation.
Otherwise, it will be automatically installed by Eclipse's package manager.

## Installing the plugin

The easiest way to install the plug-in is to choose 
`Help > Install New Software...`, and then enter 
`https://gitmoji4eclipse.gitlab.io/update-site` in the opened window.
Click on the plug-in and then on `Finish` to end the installation.
Make sure not to choose `Group items by category`.

## Editing the source code

To edit the source code, you need Eclipse's
[Plug-in Development Environment](https://www.eclipse.org/pde/).

Many tutorials can be found on the web on how to develop an Eclipse plug-in.
See e.g. 
[this web page](http://www.vogella.com/tutorials/EclipsePlugin/article.html).

This project can be used as a standalone project while you develop
your own changes.
However, you will not be able to install it without both
[the feature](https://gitlab.com/gitmoji4eclipse/feature), 
and [the update-site](https://gitlab.com/gitmoji4eclipse/update-site)
projects.

## Building the project from sources

You must clone the three projects of the plug-in, and import them into your
Eclipse PDE:

+ [the plug-in](https://gitlab.com/gitmoji4eclipse/plugin)
+ [the feature](https://gitlab.com/gitmoji4eclipse/feature)
+ [the update-site](https://gitlab.com/gitmoji4eclipse/update-site)

In the update-site project, open the file `site.xml` and click on the
`Build All` button, located in the `Site Map` tab.

This will build a local update-site, whose path is the one of the project.
You can install the built software by choosing 
`Help > Install New Software...`.
In the opened window, enter the path to your local copy of the update-site
project, click on the plug-in and then on `Finish`.
